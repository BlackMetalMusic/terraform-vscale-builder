# Vscale-prov-builder

A simple way to create vscale provider for Terraform

## Build via Dockerfile

Create an image. Provider will be compiled in process.

```git clone git@gitlab.rebrainme.com:john_doe/operations-ops3-tf-vscale.git```

```cd operations-ops3-tf-vscale/vscale-prov-builder/```

```docker build --build-arg UID=$(id -u) --build-arg GID=$(id -g) -t vscale-prov .```

Then put it to plugins dir if you run Terraform and Docker on same machine

```mkdir ~/.terraform.d/plugins```

```docker run --rm -v ~/.terraform.d/plugins:/usr/local/bin/ vscale-prov```

Otherwise it's better to use another dir instead of ~/.terraform.d/plugins and move provider via scp/rsync/etc to another machine

And remove useless image

```docker rmi vscale-prov```

## Links

* [John Doe](https://gitlab.com/BlackMetalMusic) - who wrote this
* [Vscale Provider on Github](https://github.com/burkostya/terraform-provider-vscale)
* [Vscale Provider usage examples](https://github.com/burkostya/terraform-provider-vscale/tree/bab97d7ef21cbd0a5c60f49f9e71759e06a5cc38/example)
